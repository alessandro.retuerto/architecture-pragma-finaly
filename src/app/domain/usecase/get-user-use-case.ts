import { Inject, inject, Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';

import { User } from '../models/User/user';
import { UserGateway } from '../models/User/gateway/user-gateway';

@Injectable({
  providedIn: 'root',
})
export class GetAlbumUseCases {
  constructor(private _albumGateWay: UserGateway) {}
  getAlbumById(id: String): Observable<User> {
    //TODO: En este sitio podríamos manejar las configuraciones
    //en cache
    return this._albumGateWay.getByID(id);
  }
  getAllAlbum(): Observable<Array<User>> {
    return this._albumGateWay.getAll();
  }
}
