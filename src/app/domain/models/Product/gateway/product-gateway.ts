import { Observable } from 'rxjs';
import { Product } from '../product';

export abstract class ProductGateway {
  abstract getByID(id: String): Observable<Product>;
  abstract getAll(): Observable<Array<Product>>;
  abstract saveNew(_alb: Product): Observable<void>;
}
