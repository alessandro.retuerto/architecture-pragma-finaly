import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { User } from '../../../domain/models/User/user';
import { UserGateway } from '../../../domain/models/User/gateway/user-gateway';

@Injectable({
  providedIn: 'root',
})
export class UserApiServiceWithoutDelay extends UserGateway {
  private _url = 'https://jsonplaceholder.typicode.com/albums/';
  constructor(private http: HttpClient) {
    super();
  }
  getByID(id: String): Observable<User> {
    return this.http.get<User>(this._url + id);
  }
  getAll(): Observable<User[]> {
    return this.http.get<Array<User>>(this._url);
  }
  saveNew(_alb: User): Observable<void> {
    throw new Error('Method not implemented.');
  }
}
